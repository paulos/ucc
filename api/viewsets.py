from rest_framework import mixins
from rest_framework.decorators import list_route
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from api.models import Profile
from api.serializers import ProfileSerializer, UserCreateSerializer, OwnerProfileSerializer, UpdateProfileSerializer,\
  DetailProfileSerializer


class ProfileViewSet(#mixins.CreateModelMixin,
                   mixins.RetrieveModelMixin,
                   #mixins.UpdateModelMixin,
                   #mixins.DestroyModelMixin,
                   mixins.ListModelMixin,
                   GenericViewSet):
    queryset = Profile.objects
    serializer_class = ProfileSerializer
    permission_classes = (IsAuthenticated,)

    def get_serializer_class(self):
        if self.action == 'new':
            return UserCreateSerializer
        if self.action == 'who_ami_i':
            return OwnerProfileSerializer
        if self.action == 'update_self':
            return UpdateProfileSerializer
        if self.action == 'retrieve':
            return DetailProfileSerializer
        return self.serializer_class

    def get_permissions(self):
        if self.action in ('new', 'list', 'who_am_i', 'retrieve'):
            return []
        return super().get_permissions()

    @list_route()
    def who_am_i(self, request, pk=None):
        """Returns the current user profile or an empty object if not autenticated."""
        user = request.user
        if user.is_authenticated():
            serializer = OwnerProfileSerializer(instance=user.profile, many=False)
            return Response(serializer.data)
        return Response({})

    @list_route(methods=['post'])
    def new(self, request, pk=None):
        """Created a new user. Returns the empty profile and the auth_token."""
        serializer = self.get_serializer_class()(data=request.data)
        if serializer.is_valid():
            new_user = serializer.save()
            data = OwnerProfileSerializer(instance=new_user.profile).data
            data.update({"token": new_user.auth_token.key})
            return Response(data)
        data = serializer.errors
        msg = UserCreateSerializer.mismatch_msg
        if msg in data.get('non_field_errors', []):
            data['confirmation'] = [msg]
        else:
            print(data, msg)
        return Response(data, status=422)

    @list_route(methods=['post'])
    def update_self(self, request, pk=None):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            profile = serializer.save()
            if profile is not None:
                data = OwnerProfileSerializer(instance=profile).data
                return Response(data)
        data = serializer.errors
        return Response(data, status=422)

