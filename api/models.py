from django.contrib.auth import get_user_model
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from rest_framework.renderers import DocumentationRenderer


class Topic(models.Model):
    name = models.CharField(max_length=12)
    description = models.CharField(max_length=255)

    def __str__(self):
      return self.name


class Profile(models.Model):
    user = models.OneToOneField(get_user_model())
    current_position = models.CharField(max_length=64)
    about = models.CharField("About you", max_length=255)
    favorite_topics = models.ManyToManyField(Topic, blank=True)

    def __str__(self):
      return self.user.get_full_name().strip() or self.user.username


@receiver(post_save, sender=get_user_model())
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


@receiver(post_save, sender=get_user_model())
def create_profile(sender, instance=None, created=False, **kwargs):
  if created:
    Profile.objects.create(user=instance, current_position='', about='')


