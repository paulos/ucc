from django.shortcuts import render
from django.views.generic import TemplateView


class FrontEnd(TemplateView):
    template_name = "index.html"

