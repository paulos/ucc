from django.conf.urls import url
from rest_framework import routers
from rest_framework.authtoken import views as authtoken_views

from api import viewsets
from api import views

router = routers.SimpleRouter()
router.register(r'profiles', viewsets.ProfileViewSet)

urlpatterns = [
    url(r'^token-auth/', authtoken_views.obtain_auth_token),
]

urlpatterns += router.urls
