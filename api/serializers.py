from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import PBKDF2SHA1PasswordHasher
from django.shortcuts import get_object_or_404
from rest_framework import serializers

from api.models import Profile, Topic


class UserSidecarSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ('username', 'first_name', 'last_name', 'email', 'is_superuser')


class UserDetailSerializer(serializers.ModelSerializer):
  class Meta:
    model = get_user_model()
    exclude = ('password',)


class UserCreateSerializer(serializers.ModelSerializer):
    confirmation = serializers.CharField()
    email = serializers.EmailField()
    mismatch_msg = 'Password and password confirmation must be the same.'

    def validate(self, data):
        if data['password'] != data['confirmation']:
            raise serializers.ValidationError(self.mismatch_msg)
        data.pop('confirmation')
        data['username'] = data['email']
        hasher = PBKDF2SHA1PasswordHasher()
        data['password'] = hasher.encode(data['password'], hasher.salt())
        return data

    def validate_email(self, value):
        value = value.strip().lower()
        if get_user_model().objects.filter(email=value).count():
            raise serializers.ValidationError("This email is already in use.")
        return value

    class Meta:
        model = get_user_model()
        fields = ('email', 'password', 'confirmation')


class TopicSerializer(serializers.ModelSerializer):
    class Meta:
        model = Topic
        fields = '__all__'


class ProfileSerializer(serializers.ModelSerializer):
    user = UserSidecarSerializer(many=False)
    favorite_topics = TopicSerializer(many=True)
    full_name = serializers.SerializerMethodField()

    def get_full_name(self, obj):
        full_name = " ".join([obj.user.first_name or '', obj.user.last_name or ''])
        if full_name.strip():
            return full_name
        return obj.user.username

    class Meta:
        model = Profile
        fields = ('id', 'user', 'full_name', 'current_position', 'about', 'favorite_topics')


class DetailProfileSerializer(ProfileSerializer):
  user = UserDetailSerializer(many=False)

  class Meta:
    model = Profile
    fields = ('user', 'full_name', 'current_position', 'about', 'favorite_topics')


class OwnerProfileSerializer(ProfileSerializer):
    all_topics = serializers.SerializerMethodField()

    def get_all_topics(self, obj):
        my_topics = obj.favorite_topics.all()
        return [{
                  "id": topic.id,
                  "name": topic.name,
                  "description": topic.description,
                  "prefered": topic in my_topics,
              } for topic in Topic.objects.order_by('name')]

    class Meta:
        model = Profile
        fields = ('id', 'user', 'full_name', 'current_position', 'about', 'favorite_topics', 'all_topics')


class UpdateProfileSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    first_name = serializers.CharField()
    last_name = serializers.CharField()
    current_position = serializers.CharField()
    about = serializers.CharField()
    favorite_topics = serializers.ListField(
        child=serializers.IntegerField(),
        allow_empty=False,
        min_length=1,
        max_length=6,
    )

    def validate_id(self, value):
        self.__instance = get_object_or_404(Profile, pk=value)
        request = self.context.get('request')
        if request is None:
            print(self.context)
            raise serializers.ValidationError("Invalid profile id {}".format(value))
        if self.__instance.user != request.user:
            raise serializers.ValidationError(
              "You are not logged in as {}. Only the profile onwer can update it.".format(self.__instance.user.username)
            )
        return value

    def save(self):
        if not self.is_valid():
            return

        if not self.__instance:
            return
        print(self.data)
        profile = self.__instance
        profile.user.first_name = self.data['first_name']
        profile.user.last_name = self.data['last_name']
        profile.about = self.data['about']
        profile.current_position = self.data['current_position']
        profile.save()
        profile.user.save()

        topics = Topic.objects.all()
        for topic in topics:
            if topic.id in self.data['favorite_topics']:
                profile.favorite_topics.add(topic.id)
            else:
                profile.favorite_topics.remove(topic.id)
        return profile

