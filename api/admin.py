from django.contrib import admin

# Register your models here.
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin

from api.models import Profile, Topic


class ProfileInline(admin.StackedInline):
    model = Profile
    can_delete = False
    verbose_name_plural = 'Profile'
    fk_name = 'user'


class CustomUserAdmin(UserAdmin):
    inlines = (ProfileInline, )

    def get_inline_instances(self, request, obj=None):
        if not obj:
            return list()
        return super(CustomUserAdmin, self).get_inline_instances(request, obj)


class TopicAdmin(admin.ModelAdmin):
  list_display = ('name', 'description')


class ProfileAdmin(admin.ModelAdmin):
  list_display = ('id', 'user')


user_model = get_user_model()
admin.site.unregister(user_model)
admin.site.register(user_model, CustomUserAdmin)
admin.site.register(Topic, TopicAdmin)
admin.site.register(Profile, ProfileAdmin)
