import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const apiRoot = '/api'

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  state: {
    loading: false,
    isAuthenticated: false,
    isSuperuser: false,
    token: undefined,
    user: undefined,
    profiles: []
  },
  mutations: {
    updateToken (state, token) {
      state.token = token
    },
    updateProfiles (state, profiles) {
      state.profiles = profiles
    },
    forgetLogin (state) {
      state.isAuthenticated = false
      state.token = undefined
      state.user = undefined
      state.isSuperuser = false
    },
    updateCurrentUser (state, user) {
      state.user = user
      if (user.user && user.user.is_superuser) {
        state.isSuperuser = true
      }
    },
    updateLoading (state, loading) {
      state.loading = loading
    }
  },
  actions: {
    authenticate ({commit}, creds) {
      return new Promise((resolve, reject) => {
        commit('updateLoading', true)
        Vue.http.post(apiRoot + '/token-auth/', creds)
          .then(response => {
            commit('updateToken', response.data.token)
            const headers = {
              Authorization: 'Token ' + response.data.token
            }
            Vue.http.get(apiRoot + '/profiles/who_am_i/', {}, {headers: headers})
              .then(
                response => {
                  commit('updateCurrentUser', response.data)
                },
                response => {
                  console.log(response)
                }
              )
            resolve(response)
          }, response => {
            reject(response)
          }
        ).finally(() => {
          commit('updateLoading', false)
        })
      })
    },
    refreshProfiles ({commit}) {
      commit('updateLoading', true)
      return Vue.http.get(apiRoot + '/profiles/')
        .then(response => {
          commit('updateProfiles', response.data)
        }, response => {
          console.log(response)
        }).finally(() => {
          commit('updateLoading', false)
        })
    },
    createProfile ({commit}, form) {
      return new Promise((resolve, reject) => {
        commit('updateLoading', true)
        Vue.http.post(apiRoot + '/profiles/new/', form)
          .then(response => {
            commit('updateToken', response.data.token)
            commit('updateCurrentUser', response.data)
            resolve(response)
          }, response => {
            reject(response)
          }).finally(() => {
            commit('updateLoading', false)
          })
      })
    },
    updateProfile ({commit}, form) {
      return new Promise((resolve, reject) => {
        commit('updateLoading', true)
        form.favorite_topics = []
        form.topics.forEach((topic, index) => {
          if (topic.prefered) {
            form.favorite_topics.push(topic.id)
          }
        })
        Vue.http.post(apiRoot + '/profiles/update_self/', form)
          .then(response => {
            commit('updateCurrentUser', response.data)
            resolve(response)
          }, response => {
            reject(response)
          }).finally(() => {
            commit('updateLoading', false)
          })
      })
    },
    logout ({commit}) {
      commit('forgetLogin')
    },
    loaded ({commit}) {
      commit('updateLoading', false)
    },
    loading ({commit}) {
      commit('updateLoading', true)
    }
  },
  getters: {
    isAuthenticated (state) { return state.token !== undefined },
    profiles (state) { return state.profiles },
    currentUser (state) { return state.user },
    currentToken (state) { return 'Token ' + state.token },
    currentUserLabel (state) {
      if (state.token && state.user && state.user.full_name) {
        return state.user.full_name
      } else {
        return 'Not logged in'
      }
    },
    loading (state) { return state.loading },
    isSuperuser (state) { return state.isSuperuser }
  }
})

