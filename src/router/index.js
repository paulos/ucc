import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Login from '@/components/Login'
import Logout from '@/components/Logout'
import Profile from '@/components/Profile'
import SignUp from '@/components/SignUp'
import UserDetail from '@/components/UserDetail'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import store from '../store'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/sign-up',
      name: 'SignUp',
      component: SignUp,
      beforeEnter: (to, from, next) => {
        if (store.getters.isAuthenticated && from.name === 'Login') {
          next({name: 'Home'})
        }
        next()
      }
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/logout',
      name: 'Logout',
      component: Logout,
      beforeEnter: (to, from, next) => {
        if (from.name === 'Login') {
          next({name: 'Home'})
        }
        next()
      }
    },
    {
      path: '/profile',
      name: 'Profile',
      component: Profile,
      beforeEnter: (to, from, next) => {
        if (!store.getters.isAuthenticated) {
          next({name: 'Login'})
        } else {
          next()
        }
      }
    },
    {
      path: '/user/:id',
      name: 'UserDetail',
      component: UserDetail
    }
  ]
})
