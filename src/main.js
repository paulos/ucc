// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueResource from 'vue-resource'
import Vuex from 'vuex'
import Gravatar from 'vue-gravatar'
import Icon from 'vue-awesome/components/Icon'
import BootstrapVue from 'bootstrap-vue'
import VueCookie from 'vue-cookie'
import App from './App'
import router from './router'
import store from './store'
import 'vue-awesome/icons'

Vue.use(BootstrapVue)
Vue.use(VueResource)
Vue.use(Vuex)
Vue.use(VueCookie)

Vue.component('v-gravatar', Gravatar)
Vue.component('fa-icon', Icon)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  template: '<App/>',
  components: { App }
})

Vue.http.headers.common['x-csrftoken'] = Vue.cookie.get('csrftoken')

Vue.http.interceptors.push((request, next) => {
  if (store.getters.isAuthenticated) {
    request.headers.set('Authorization', store.getters.currentToken)
  }
  next()
})
