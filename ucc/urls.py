"""ucc URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from rest_framework.documentation import include_docs_urls
from rest_framework.permissions import AllowAny
from api import views as api_views

urlpatterns = [
    url('admin/', admin.site.urls),
    url('docs/', include_docs_urls(title='Udemy Code Challenge', authentication_classes=[], permission_classes=[], public=True)),
    url('api/', include('api.urls')),
    url(r'$', api_views.FrontEnd.as_view()),
]
