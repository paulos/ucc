# ucc

This toy project is my take on the Udemy code challenge.


## Bootstrap

This command should create a virtualenv inside the project and install dependencies:

    python3 bootstrap.py

If it fails to bootstrap, there is an instance running at:
 http://xtend.com.br:8080/ 

## Design decisions

Backend is using Django Rest Framework. Opted for using sqlite as the database -
I know MySQL is supposed to be installed but I think sqlite is better for a
toy project like this. Database was populated with randomly generated sample 
data using the Faker library - the fact that some fake email addresses had
gravatars is just coincidence.

Admin is using the default Django admin app. There is a menu entry if you are
logged in as an administrator.

When not logged in, there are buttons for login and signup. When logged
in there is a submenu with entries for logout and profile editing. 

Both Backend and Frontend are in the same repo. Again this is OK for a toy 
SPA but not for a big project.

Frontend is using Vue and Bootstrap 4. It is not React, but is not too far 
from it: both frameworks use a virtual DOM and Vuex is quite like Flux. 
Why Vue? There are several reasons:
 
* same as why Django instead of Rails: I like opinionated frameworks because
  developer onboarding is way easier when every project follows the same
  conventions.
* Vue is on the rise, look at the npm statistics.
* I can off-load a Vue template to any UI designer but I don't trust JSX
  to non-programmers.
* React and Vue both excel at handling dumb components: small, stateless 
  functions that receive an input and return elements as output.
* Vue can use JSX as well, but I would ratter keep my HTML and Javascript
 aparted.
  
I have nothing against React and would consider it for a bigger project.


## Frontend Build Setup

The bootstrap command should take care of this step. If the bootstrap fail 
and you want to build the frontend manually in order to check error messages:

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
