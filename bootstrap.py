#!/usr/bin/env python
import os
import sys

assert sys.version_info[0] > 2, 'Sorry pal, this is Python3 only. Can you try again with "python3 bootstrap.py"?'
assert sys.version_info[1] > 4, 'Your python3 looks too old. Python >= 3.5 required.'

import pip

BASE_PATH = os.path.dirname(os.path.realpath(__file__))

def is_venv():
    return (hasattr(sys, 'real_prefix') or
            (hasattr(sys, 'base_prefix') and sys.base_prefix != sys.prefix))


def install_and_import(package):
    import importlib
    try:
        importlib.import_module(package)
    except ImportError:
        pip.main(['install', package])
    finally:
        globals()[package] = importlib.import_module(package)


def execfile(filename, global_vars=None, local_vars=None):
    with open(filename) as f:
        code = compile(f.read(), filename, 'exec')
        exec(code, global_vars, local_vars)


def create_venv():
    try:
        import virtualenv
    except ImportError:
        print("Installing virtualenv in your global environment.")
        install_and_import('virtualenv')

    venv_dir = os.path.join(BASE_PATH, '.venv')
    virtualenv.create_environment(venv_dir)
    execfile(os.path.join(venv_dir, 'bin', 'activate_this.py'))


def install_requirements():
    pip = sh.Command(os.path.join(BASE_PATH, '.venv/bin/pip'))
    pip('install', '-r', 'requirements.txt')


def run_npm():
    assert sh.node('--version').strip() >= 'v9.3', 'Sorry, your node looks old. This needs >= 9.3'
    sh.npm('install')
    sh.npm('run', 'build')


def main():
    create_venv()

    os.chdir(BASE_PATH)
    install_and_import('sh')

    install_requirements()
    run_npm()

    sh.ln('-s', os.path.join(BASE_PATH, 'dist/static'), os.path.join(BASE_PATH, 'api', 'static'))
    sh.ln('-s', os.path.join(BASE_PATH, 'dist/index.html'), os.path.join(BASE_PATH, 'api', 'templates', 'index.html'))

    print("""If everything went well, you can run:
    source .venv/bin/activate
    python manage.py runserver 0.0.0.0:8080
    """)

if __name__ == '__main__':
    main()




